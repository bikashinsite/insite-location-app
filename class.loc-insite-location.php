<?php

require_once( LOC_INSITE_INT__LIB_DIR . '/class.Loc_InSite_Handler_Interface.php' );
require_once( LOC_INSITE_INT__LIB_DIR . '/Handlers/class.InSite_LOC_Handler.php' );

/**
 * Class Loc_InSite_Location
 */
class Loc_InSite_Location {

	/**
	 * @var string
	 */
	const LOC_SLUG = 'location';

	/**
	 * @var int
	 */
	const LOC_HANDLER_KEY = 1001;

	/**
	 * @var WP_Query|null
	 */
	private $wp_query;
	
	/**
	 * Loc_InSite_Location constructor.
	 * @param WP_Query|null $wp_query
	 * @param array $insiteShortcodes
	 */
	public function __construct($wp_query, array $insiteShortcodes) {
		$this->wp_query = $wp_query;
		$this->register($insiteShortcodes);
	}

	/**
	 * Register all event handlers here
	 * @param array $insiteShortcodes
	 */
	private function register(array $insiteShortcodes) {
		//add_action('init', array($this, 'interactive_rewrite'));
		//register all insite shortcodes
		foreach ($insiteShortcodes as $sc) {
			Loc_InSite_Shortcode::registerShortcode($sc);
		}
	}

	/**
	 * Register and enqueue any relevant js and css scripts
	 */
	public function insite_scripts() {
		// register your script location, dependencies and version
		wp_register_script('InSite-Int-Loc-js', LOC_INSITE_INT__JS, array(), null, false);
		
		// enqueue the script
		wp_enqueue_script('InSite-Int-Loc-js');

		wp_register_style( 'InSite-Int-Loc-css', LOC_INSITE_INT__CSS );

		wp_enqueue_style( 'InSite-Int-Loc-css' );
	}

	/**
	 * @param int $handlerType
	 * @return Loc_InSite_Handler_Interface
	 */
	public static function getInSiteShortCodeHandler($handlerType) {
		$returnObj = null;

		switch ($handlerType) {
			case self::LOC_HANDLER_KEY:
				$returnObj = new InSite_LOC_Handler();
				break;
			default:
				break;
		}

		return $returnObj;
	}
}
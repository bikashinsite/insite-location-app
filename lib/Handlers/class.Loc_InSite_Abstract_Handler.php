<?php

require_once( LOC_INSITE_INT__LIB_DIR . '/class.Loc_InSite_Handler_Interface.php' );

abstract class Loc_InSite_Abstract_Handler implements Loc_InSite_Handler_Interface {

	public abstract function render();

	/**
	 * render out the necessary css and js for the insite shortcodes
	 */
	protected function renderInSiteIncludes() {
		$js = LOC_INSITE_INT__JS;
		$css = LOC_INSITE_INT__CSS;
		wp_register_script('is-src', $js, array(), false, true);
		wp_register_style('is-style', $css);

		wp_enqueue_script('is-src');
		wp_enqueue_style('is-style');
	}
}
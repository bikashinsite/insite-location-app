<?php
require_once( LOC_INSITE_INT__LIB_DIR . '/Handlers/class.Loc_InSite_Abstract_Handler.php' );

class InSite_LOC_Handler extends Loc_InSite_Abstract_Handler {
	public function render() {
		echo "<div id='InSiteLocation'></div>";
		$this->renderInSiteIncludes();
	}
}
<?php

/**
 * Wrapper around creating and registering shortcodes
 *
 * Class Loc_InSite_Shortcode
 */
class Loc_InSite_Shortcode {

	/**
	 * @var string
	 */
	private $shortcodeTag;

	/**
	 * @var Callable|Closure
	 */
	private $shortcodeHandler;

	/**
	 * @param Loc_InSite_Shortcode $shortcode
	 * @return void
	 */
	public static function registerShortcode(Loc_InSite_Shortcode $shortcode) {
		add_shortcode($shortcode->getTag(), $shortcode->getHandler());
	}

	/**
	 * Loc_InSite_Shortcode constructor.
	 * @param string $shortcodeTag
	 * @param Callable|Closure $shortcodeHandler
	 */
	public function __construct($shortcodeTag, $shortcodeHandler) {
		$this->shortcodeTag = $shortcodeTag;
		$this->shortcodeHandler = $shortcodeHandler;
	}

	/**
	 * @return string
	 */
	public function getTag() {
		return $this->shortcodeTag;
	}

	/**
	 * @return Callable
	 */
	public function getHandler() {
		return $this->shortcodeHandler;
	}
}
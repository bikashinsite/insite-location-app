<?php
/*
Plugin Name: InSite Location App
Plugin URI: http://insitelogic.com.au/
Description: InSite Logic website plugin, allows for the installation of interactive  Amenity/Locations shortcode: [insite-interactive-loc]
Version: 2.0.0
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

// general config values
define( 'INSITE_INT_VERSION', '2.0.0' );
define( 'INSITE_INT__MINIMUM_WP_VERSION', '3.2' );
define( 'LOC_INSITE_INT__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'LOC_INSITE_INT__LIB_DIR', LOC_INSITE_INT__PLUGIN_DIR . 'lib' );

define( 'LOC_INSITE_INT__JS','https://storage.googleapis.com/interactive-maplestone/assets/location/app/main.f39c085e.js');
define( 'LOC_INSITE_INT__CSS','https://storage.googleapis.com/interactive-maplestone/assets/location/app/main.a0b9c9cc.css');

// load globals from wordpress
global $wp_query;

// require initial files
require_once( LOC_INSITE_INT__PLUGIN_DIR . 'class.loc-insite-location.php' );
require_once( LOC_INSITE_INT__LIB_DIR . '/class.Loc_InSite_Shortcode.php' );

// register activation and de-activation hooks
register_activation_hook( __FILE__, array( 'InSiteLoc', 'plugin_activation' ) );
register_deactivation_hook( __FILE__, array( 'InSiteLoc', 'plugin_deactivation' ) );

// create shortcodes
$shortcodes = array();

$shortcodes[] = new Loc_InSite_Shortcode('insite-interactive-loc', function () {
	//use factory method to get appropriate implementation
	$handler = Loc_InSite_Location::getInSiteShortCodeHandler(
		Loc_InSite_Location::LOC_HANDLER_KEY
	);

	//call the render function
	$handler->render();
});

$insite = new Loc_InSite_Location($wp_query, $shortcodes);